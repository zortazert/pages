#+TITLE: Color Pickers
#+HTML_HEAD: <link rel='stylesheet' type='text/css' href='styles.css' />
#+OPTIONS: html-style:nil toc:nil num:nil 

* Color pickers in software
This is a tutorial on how to pick colors. Specifically with [[https://en.wikipedia.org/wiki/Color_picker][Color Pickers]] I am by no means an expert in color but I would like to help people pick colors at least the way I like to do it.

[[https://upload.wikimedia.org/wikipedia/commons/thumb/1/1f/Gimp_color_selector_03.gif/330px-Gimp_color_selector_03.gif]]

Up above is GIMP's color picker I found on wikipedia. As you can see there are A LOT of color pickers available in a lot of software. The Rectangle or the Triangle one's I think are easier because you see the color spectrum.
** Color slider
The color slider shows a vertical line gradient between ordinary colors like Red, Blue and Yellow etc. These are kind of like crayons. They are sort of bland everyone knows what they are.
** Color Canvas
Next to the color slider is the color canvas. It shows the color you picked on the slider as a gradient with the color black. You can also get some white color from here. Basicly on this color canvas you can kind of edit the color to be super *EPIC*.
* General Rules (Least to Most Importance)
** Black & White
White =#ffffff= and Black =#000000= are very *extreme* colors they probably do have a usecase but rarely. They aren't very appealing colors. Also in real life you generally don't see something so dark or bright as =#ffffff= or =#000000=. (depends on the quality of your screen). So when you want a *BLACK* color tone it down by making it a bit more grey.

#+attr_html: :width 300px
[[https://zortazert.codeberg.page/images/Drip%20+%20chadoku.png]]

Here is an example of Chadoku I made wearing sun glasses. The sunglasses have a bit of grey in them. In my opinion this is much better than if I put =#000000=.
** Saturation
** Value
** Contrast
* Extra recources
- A video by Brad Colbow about contrast in colors https://invidio.xamh.de/watch?v=b-PqO-ILcYo
- Another video by Brad Colbow about color theory https://invidio.xamh.de/watch?v=NBg3GjrcMF4
- Wikipedia's article on Color Pickers https://en.wikipedia.org/wiki/Color_picker

* License
#+BEGIN_EXPORT html 
<hr> 
<footer> 
<a rel='license' href='http://creativecommons.org/licenses/by-sa/4.0/'><img alt='Creative Commons License' style='border-width:0' width='88' height='31' src='../images/cc-by-sa.png' /></a><br> 
Unless otherwise noted, all content on this website is Copyright Zortazert 2021-2022 and is licensed under <a rel='license' href='http://creativecommons.org/licenses/by-sa/4.0/'>CC BY-SA 4.0</a>. 
</footer> 
#+END_EXPORT 
