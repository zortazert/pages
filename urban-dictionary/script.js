document.querySelector('#search').addEventListener("click",getSearchResults);

function capitalizeFirstLetter(string){
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function boldify(string){
    return string.replace(/\[/g, "<b>").replace(/\]/g, "</b>")
}

function getSearchResults(e) {
    const name = document.querySelector("#searchInput").value.toLowerCase();
	fetch("https://api.allorigins.win/raw?url=https://api.urbandictionary.com/v0/define?term="+name)
	.then((response) => response.json())
	.then((data) => {
			let list = `
			
			`;
		data.list.forEach(function(result){
		    date = new Date(result.written_on)
			list += `
		<div class="searchResultInfo">
			<h2>${result.word}</h2>
			<p>${boldify(result.definition)}</p>
			<p>${boldify(result.example)}</p>
			<p><b>by ${result.author} ${date.toLocaleString('default', { month: 'long'})} ${date.getDate()}, ${date.getFullYear()}</b></p>
		<hr>
		</div>
		`;
		})
		document.querySelector(".searchResult").innerHTML = list
	})
	.catch((err) => {
		document.querySelector(".searchResult").innerHTML = `
<div>
<p>Could not find this 😟. Try again.</p>
</div>`;
	    console.log(err);
	});
	e.preventDefault();
}
