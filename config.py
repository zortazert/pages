# RSS file
rssfile = 'rss.xml'
# Where blogs go on your site
website_dir = "https://zortazert.codeberg.page/blog/"
# Where blogs are stored physically on your computer
blog_dir = "C:\\SGZ_Pro\\Hobbys\\Writing\\Org\\pages\\blog\\"
# Where the blog file is stored on your computer:
blog_file = "C:\\SGZ_Pro\\Hobbys\\Writing\\Org\\pages\\index.html"
# Prefered markup language. org or md
markup = "org"
