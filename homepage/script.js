// global variables
// CORS errors are cringe
const pokedex = "https://codeberg.org/zortazert/pages/raw/branch/main/homepage/pokemon.json"
const castforms = {
	"sunny": "https://archives.bulbagarden.net/media/upload/thumb/9/90/0351Castform-Sunny.png/120px-0351Castform-Sunny.png",
	"snowy": "https://archives.bulbagarden.net/media/upload/thumb/3/34/0351Castform-Snowy.png/120px-0351Castform-Snowy.png",
	"rainy": "https://archives.bulbagarden.net/media/upload/thumb/a/ab/0351Castform-Rainy.png/120px-0351Castform-Rainy.png",
	"normal": "https://archives.bulbagarden.net/media/upload/thumb/f/ff/0351Castform.png/120px-0351Castform.png"
}
const phases = {
    "New Moon": "🌑",
    "Waxing Crescent": "🌒",
    "First Quarter": "🌓",
    "Waxing Gibbous": "🌔",
    "Full Moon": "🌕",
    "Waning Gibbous": "🌖",
    "Last Quarter": "🌗",
    "Waning Crescent": "🌘",
}
var latlon = "";

// GEO location
function successCallback(position) {
  console.log(position);
  latlon = `${position.coords.latitude},${position.coords.longitude}`;
};

const errorCallback = (error) => {
  console.log(error);
};

navigator.geolocation.getCurrentPosition(successCallback, errorCallback);

fetch(`https://wttr.in/${latlon}?format=j1`)
.then((response) => response.json())
.then((data) => {
    var description = data.current_condition[0].weatherDesc[0].value.toLowerCase();
    console.log(description)
    var form = "";
    if (description.includes("sun")) {
	    form = "sunny";
    } else if (description.includes("cold")) {
	    form = "snowy";
    } else if (description.includes("rain")) {
	    form = "rainy";
    } else {
	    form = "normal"
    }
    console.log(form)
    document.querySelector(".weather").innerHTML = `<img src="${castforms[form]}">`;
    moonPhase = data.weather[0].astronomy[0].moon_phase;
    console.log(moonPhase);
    document.querySelector(".phase").innerHTML = `<p>${phases[moonPhase]} ${moonPhase}</p>`;
})
.catch((err) => {
	document.querySelector(".info").innerHTML = `
<div>
<p>Could not find this 😟. Try again.</p>
</div>`;
	    console.log(err);
});

function typeFormatter(types) {
    var typeFormat = [];
    // https://www.w3schools.com/js/js_loop_for.asp
    // https://www.freecodecamp.org/news/how-to-capitalize-words-in-javascript/
    // https://stackoverflow.com/questions/351409/how-to-append-something-to-an-array
    for (let i = 0; i < types.length; i++) {
	typeFormat.push(types[i][0].toUpperCase() + types[i].substring(1));
    }
    return typeFormat.join(", ")
}

fetch("https://api.allorigins.win/raw?url="+encodeURIComponent(pokedex))
.then((response) => response.json())
.then((data) => {
    const value = data[Math.floor(Math.random() * Object.keys(data).length)];
    console.log(value)
    document.querySelector(".image").innerHTML = `<img src="${value["ThumbnailImage"]}">`;
    document.querySelector(".info").innerHTML = `
<p><b>Number</b>: #${value["number"]}</p>
<p><b>Name</b>: ${value["name"]}</p>
<p><b>Type</b>: ${typeFormatter(value["type"])}</p>
<p><b>Weakness</b>: ${typeFormatter(value["weakness"])}</p>
<p><b>Abilities</b>: ${value["abilities"]}</p>
<p><b>Weight</b>: ${value["weight"]}</p>
<p><b>Height</b>: ${value["height"]}</p>
<p><b>Link</b>: <a href="https://pokemon.com${value["detailPageURL"]}">https://pokemon.com${value["detailPageURL"]}</a>
`;
})
.catch((err) => {
	document.querySelector(".info").innerHTML = `
<div>
<p>Could not find this 😟. Try again.</p>
</div>`;
	    console.log(err);
});
