# blog.py
# A general purpose blog rss feed generator.
# Supports input .md, .org and basic .html files.

import os.path
import time
import sys
import re
from config import *
from pathlib import Path

def mini_help():
    print('''An incorrect or no argument was passed.
+---------------+---------------+------------------+
|Short options: | Long options: |     Meaning:     |
+---------------+---------------+------------------+
|               |               | New File in Blog |
|   -g          | --generate    | Get NEW RSS file |
|   -h          | --help        | Full Help        |
|   -e          | --export      | Export org files |
+---------------+---------------+------------------+''')
if len(sys.argv) == 1:
    files = os.listdir(blog_dir)
    paths = [os.path.join(blog_dir, basename) for basename in files]
    genfile = max(paths, key=os.path.getctime)
    print(f"Pressing enter the file will be {genfile}")
    file = input("File: ")
    print("---")
    if file == "":
        file = genfile

    try:
        with open(file) as f:
            text = f.read()
    except:
        print("ERROR: Can not find/read file")
        quit()

    # Generate RSS description
    if file.endswith(".md") == True:
        import markdown
        description = markdown.markdown(text).replace("\n","")
    elif file.endswith(".org") == True:
        from orgpython import to_html
        description = to_html(text, toc=False, offset=0, highlight=True).replace("\n","")
        print(description)
    elif file.endswith(".html") == True:
        # This is only for very basic html documents. More complicated
        # ones, probably won't work.
        description = text.replace("\n","")
        description = re.sub("<head>.*?</head>","",description)
        description = description.replace("<body>","").replace("</body>","").replace("<!DOCTYPE html>","").replace("</html>","")
    else:
        print("ERROR: This file extension is not supported :(")
        quit()

    # Get other necassary information
    gentitle = re.findall("<h1.+?>.+?</h1>", description)
    gentitle = gentitle[0].replace("&#39;","")
    clean = re.compile('<.*?>')
    gentitle = re.sub(clean, '', gentitle)
    print(f"Press enter and the title will be \"{gentitle}\"")
    title = input("Title: ")
    if title == "":
        title = gentitle

    print(f"---\nPress enter and the article link will be {website_dir}{file.replace(blog_dir,'')}")
    link = input("Link: ")
    if link == "":
        link = f"{website_dir}{file.replace(blog_dir,'')}"

    date = str(time.ctime(os.path.getmtime(file)))
    #from time import gmtime, strftime
    #date = str(strftime("%a, %d %b %Y %X"))

    try:
        with open(rssfile) as r:
            text = r.read()
            text = text.splitlines()
            text = text[:-2]
            text = '\n'.join(text)
    except:
        print("ERROR: could not find/read RSS file")

    with open(rssfile, "w") as w:
        w.write(text)
    with open(rssfile, 'a') as f:
        f.write(f"""
<item>
<title>{title}</title>
<link>{link}</link>
<guid>{link}</guid>
<pubDate>{date}</pubDate>
<description>
<![CDATA[ {description} ]]>
</description>
</item>

</channel>
</rss>""")

    f = open('rss.xml', 'r')
    content = f.read()
    print(content)
    f.close()

    with open(blog_file) as r:
        text = r.read()
    # THE IKSVO BASED REPLACE "the only form of regex I actually use
    # is .replace" --iksvo
    replacement = f"""<!-- placeholder -->
<li><a href=\"{link}\">{title}<a></li>"""
    text = text.replace("<!-- placeholder -->",replacement)
    with open(blog_file, "w") as w:
        w.write(text)

elif sys.argv[1] == "-g" or sys.argv[1] == "--generate":
    try:
        file = open(rssfile)
        print(f"THIS WILL OVERWIRTE THE CURRENT {rssfile} FILE")
    except:
        print(f"{rss.xml} doesn't exist, going to create a new one.")

    rsstitle = input("Title for your rss feed: ")
    rssdescription = input("---\nDescription for your rss feed: ")
    rsslanguage = "en-us"
    rsssite = input("---\nWebsite: ")
    print("---\nIf you have a image/favicon link. Press enter for no image/favicon")
    rssimage = input("image/favicon link: ")
    if rssimage == "":
        rssimage = ""

    with open(rssfile, "w") as f:
        f.write(f"""<?xml version=\"1.0\" encoding=\"utf-8\"?>
<rss version=\"2.0\">
<channel>
<title>{rsstitle}</title>
<description>{rssdescription}</description>
<language>{rsslanguage}</language>
<link>{rsssite}</link>""")
        if rssimage == "":
            pass
        else:
            f.write(f"""
<image>
<title>{rsstitle}</title>
<url>{rssimage}</url>
</image>""")
        f.write("""

</channel>
</rss>""")
    print("""
Now, you should have a working rss file to add blog entries to.
python blog.py FILE to add new content to the rss file.""")

elif sys.argv[1] == "-h" or sys.argv[1] == "--help":
    print("""Stetup:
- Make sure you have both this script \"blog.py\" and also another file called \"config.py\"
- Change stuff in config.py to work for your setup
- If you don't have a RSS/rss.xml file you can run \"python blog.py -g\" or \"python blog.py --generate\". Answer the questions and now you have a RSS file. WOW!
- In your index.html file write \"<!-- placeholder -->\" for where you want the link to the article to be.
- Run \"python blog.py\" to add a new entry. Answer the questions. Press enter and you can use geussed file, link and title.
- Your done, you now have rss!

Article files supported:
- .md with the markdown extention
- .org with the orgpython extention
- .html very simple html documents that follow a certain format

Other options in this program:
-g as mentioned a bit in the setup will initialize a rss.xml file for you.
-e will export documents in the blog_dir into html. Requires pandoc.

This program is kind of for my setup and how I manage my website. However you can try to contact me on matrix @trueauracoral:tchncs.de for help in this script.""")
    quit()
elif sys.argv[1] == "-e" or sys.argv[1] == "--export":
    files = sorted(Path(blog_dir+markup).iterdir(), key=os.path.getmtime)
    for file in files:
        export = str(file).replace(f".{markup}","").replace(f"{markup}\\","").replace(f"{markup}/","")
        print(export)
        command = f'pandoc -s -c {blog_dir}styles.css {file} -o {export}.html --highlight-style=tango'
        os.system(command)
else:
    mini_help()
    quit()
